<?php

return [
	'dev' => 'En cours de développement',
	'test' => 'À tester',
	'bugfix' => 'Maintenance active',
	'security-fix' => 'Correctifs de sécurité',
	'eol' => 'Fin de vie',
];
