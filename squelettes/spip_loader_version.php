<?php

use Spip\Compilateur\Noeud\Champ;

function get_spip_loader_version(): string
{
    include_spip('inc/distant');
    $version = recuperer_url_cache('https://get.spip.net/version');
	if (is_array($version) && !empty($version['page'])) {
		return $version['page'];
	}

    return '';
}

function balise_SPIP_LOADER_VERSION_dist(Champ $p): Champ
{
	$p->code = 'get_spip_loader_version()';

	return $p;
}
