<?php

use Spip\Compilateur\Noeud\Boucle;

function iterateur_supported_versions(Boucle $loop): Boucle
{
	$loop->iterateur = 'IterateurSV';

	$loop->show = [
		'field' => [
			'id' => 'INTEGER',
			'branch' => 'STRING',
			'last_version_release' => 'STRING',
			'last_url_package_release' => 'STRING',
			'follow_up' => 'STRING',
			'php_min' => 'STRING',
			'php_max' => 'STRING',
			'published' => 'STRING',
			'id_article' => 'INTEGER',
		],
	];

	return $loop;
}

/**
 * @param Boucle[] $loops
 */
function critere_supported_versions_maintained(string $loopId, array &$loops): void
{
	$loop = &$loops[$loopId];
	$loop->hash .= '
	$command[\'args\'] = [
		[
			\'filter\' => \'maintained\',
			\'published\' => $Pile[$SP][\'published\'] ?? (getDateArticle(intval($Pile[0][\'id_article\'] ?? 0))),
			\'articleId\' => $Pile[0][\'id_article\'] ?? null,
		],
	];
';
}

function getDateArticle(int $articleId): string
{
	if ($articleId === 0) {
		return '';
	}

	$published = sql_fetsel('date', 'spip_articles', 'id_article='.strval($articleId));
	if (!isset($published['date']) || str_starts_with($published['date'], '0')) {
		return '';
	}

	return (new DateTimeImmutable($published['date']))->format('Y-m-d');
}

function balise_TRANSLATED_FOLLOW_UP(Champ $field): Champ
{
    $field->code = '_T(\'iterateur-sv:\'.(' . champ_sql('follow_up', $field) . ' ?? \'\'))';

    return $field;
}

function compute_php_compatibility(string $phpMin, string $phpMax): string
{
    $phpMin = $phpMin == '' ? '?' : 'PHP&nbsp;' . $phpMin;
    $phpMax = $phpMax == '' ?: '&nbsp;&agrave;&nbsp;PHP&nbsp;' . $phpMax;

    return $phpMin . $phpMax;
}

function balise_PHP_COMPATIBILITY(Champ $field): Champ
{
    $field->code = "\n\t" . 'compute_php_compatibility(
        ' . champ_sql('php_min', $field) . ' ?? \'\',
        ' . champ_sql('php_max', $field) . ' ?? \'\'
    )';

    return $field;
}

class IterateurSV implements Iterator
{
	private int $position = 0;

	/** @var array<mixed> */
	private array $branches = [];

	/** @var array<mixed> */
	private array $digest = [];

	private string $published;

	private string $filter;

	private int $articleId;

	/**
	 * @param array{published?:string,filter?:string,articleId?:int} $filters
	 */
	public function __construct(array $filters = [])
	{
		$this->published = (new DateTimeImmutable($filters['published'] ?? ''))->format('Y-m-d');
		$this->filter = $filters['filter'] ?? 'unknown';
		$this->articleId = $filters['articleId'] ?? 0;

		// Client HTTP
		include_spip('inc/distant');
		$releases = recuperer_url_cache('https://www.spip.net/releases.json');
		if (is_array($releases) && !empty($releases['page'])) {
			$this->branches = (array) json_decode($releases['page'], true, 512, JSON_THROW_ON_ERROR);
		}

		// Filters
		$published = $this->published;
		if ($this->filter === 'maintained') {
			$this->branches = array_reverse(array_filter(
				$this->branches,
				function ($branch) use ($published) {
					$followUp = ''; # TODO dev, test
					if (isset($branch['eol']) && $branch['eol'] <= $published) {
						$followUp = 'eol';
					} elseif (isset($branch['active_support']) && $branch['active_support'] <= $published) {
						$followUp = 'security-fix';
					} elseif (isset($branch['initial_release']) && $branch['initial_release'] <= $published) {
						$followUp = 'bugfix';
					}

					return in_array($followUp, ['bugfix', 'security-fix']);
				}
			));
		}

		// Production du digest
		$this->digest = array_map(
			function ($branch) use ($published) {
				static $id = 0;
				$releases = array_filter(
					$branch['releases'] ?? [],
					function ($release) use ($published) {
						$released = preg_replace(',[ |T].*$,', '', $release['released_at']);

						return $released <= $published;
					}
				);
				usort($releases, function ($a, $b) {
					return $a['released_at'] > $b['released_at'];
				});
				$release = array_pop($releases);
				$lastVersionRelease = $release['version'] ?? '';
				$download = $release['download'] ?? [];
				$download = array_pop($download);
				$lastUrlPackageRelease = $download['url'] ?? '';

				$followUp = ''; # TODO dev, test
				if (isset($branch['eol']) && $branch['eol'] <= $published) {
					$followUp = 'eol';
				} elseif (isset($branch['active_support']) && $branch['active_support'] <= $published) {
					$followUp = 'security-fix';
				} elseif (isset($branch['initial_release']) && $branch['initial_release'] <= $published) {
					$followUp = 'bugfix';
				}

				$phpMin = $phpMax = '';
				if (isset($branch['technologies']['require']['php'])) {
					$phpMin = (string) reset($branch['technologies']['require']['php']);
					$phpMax = (string) end($branch['technologies']['require']['php']);
					if ($phpMax == $phpMin) {
						$phpMax = '';
					}
				}

				return [
					'id' => $id++,
					'branch' => $branch['branch'],
					'last_version_release' => $lastVersionRelease,
					'last_url_package_release' => $lastUrlPackageRelease,
					'follow_up' => $followUp,
					'php_min' => $phpMin,
					'php_max' => $phpMax,
				];
			},
			$this->branches
		);
	}

	public function current(): mixed
	{
		return array_merge(
			[
				'published' => $this->published,
				'id_article' => $this->articleId
			],
			$this->digest[$this->position]
		);
	}

	public function valid(): bool
	{
		return isset($this->digest[$this->position]);
	}

	public function next(): void
	{
		++$this->position;
	}

	#[\ReturnTypeWillChange]
	public function key(): mixed
	{
		return $this->position;
	}

	public function rewind(): void
	{
		$this->position = 0;
	}
}
